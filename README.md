# DragAndDropExample

## Description

In this example we have [workers](src/app/worker/worker.ts) and corresponding [shifts](src/app/worker/shift.ts). Every shift is assigned to one worker and one worker can have multiple shifts. A shift contains a date, start-, endtime and a break. Currently, dummy data for workers and shifts are generated through the [dummy-data-service](src/app/worker/dummy-data.service.ts).

Every development should be done in the app.component-files. The dummy-data-service is already injected into the component.  

The project uses TypeScript a superset of JavaScript, which extends it by types and classes. The typing is optional so it is possible to write normal JavaScript inside TypeScript. 

For handling dates the project uses Moment.js. For persisting dates they saved as strings in the format `YYYY-MM-DD`.

## Development server
Run `npm start` for a dev server. Navigate to `https://localhost:4205/`. The app will automatically reload if you change any of the source files.

## Tasks

Starting situation: 

![starting situation](images/Main-Screen.png)

### Date selection

When a date is selected the available workers should be filtered. The available workers can be derived from the available shifts. 
The available workers should be displayed. 

![date selected](images/Selected-Date-available-Workers.png)

Documentation: 

- [Datepicker](https://material.angular.io/components/datepicker/overview)
- [Moment.js](https://momentjs.com/docs/)
- [Arrays](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array)
- [Filter-Operations](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)

### Distinguish between active and non-active workers

The available workers should be devided into two disjoint sets. One set should be the active workers and one the non-active workers. Via drag-and-drop a worker should be changed from active to non-active and vice versa. Initially all available workers should be active. 

![active non-active workers](images/Active-non-active-Workers.png)

- [Drag and Drop](https://material.angular.io/cdk/drag-drop/overview)