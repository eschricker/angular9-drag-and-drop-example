import { Injectable } from '@angular/core';
import { Worker } from './worker';
import { Shift, DATE_FORMAT } from './shift';

import * as faker from 'faker';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DummyDataService {

  private readonly _workers: Array<Worker> = [];
  private readonly _shifts: Array<Shift> = [];

  constructor() {
    faker.locale = 'es_MX';

    for (let i = 0; i < 10; i++) {
      this._workers.push({
        workerID: i,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName()
      });
    }

    const earlyShift: Shift = {
      workerID: -1,
      date: 'invalid',
      begin: '08:00',
      end: '16:00',
      break: {
        begin: '09:00',
        end: '15:00',
        duration: 30
      }
    };

    const lateShift: Shift = {
      workerID: -1,
      date: 'invalid',
      begin: '14:00',
      end: '22:00',
      break: {
        begin: '15:00',
        end: '21:00',
        duration: 30
      }
    };

    for (let i = 0; i < 10; i++) {
      const date = moment().add(i, 'days');
      this._workers.forEach(w => {
        const active: boolean = faker.random.boolean();

        if (active) {
          const early: boolean = faker.random.boolean();
          this._shifts.push({
            ... (early ? earlyShift : lateShift),
            workerID: w.workerID,
            date: date.format(DATE_FORMAT)
          });
        }
      });
    }
  }

  shifts(): Array<Shift> {
    return this._shifts;
  }

  workers(): Array<Worker> {
    return this._workers;
  }
}
