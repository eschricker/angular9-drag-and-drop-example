import { TimeWindow } from './time-window';
import { Break } from './break';

export const DATE_FORMAT: string = 'YYYY-MM-DD';

export interface Shift extends TimeWindow {
    workerID: number;
    break: Break;
    date: string;
}
