import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DummyDataService } from './dummy-data.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    DummyDataService
  ]
})
export class WorkerModule { }
