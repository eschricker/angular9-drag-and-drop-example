export interface Worker {
    workerID?: number;
    firstName: string;
    lastName: string;
}
