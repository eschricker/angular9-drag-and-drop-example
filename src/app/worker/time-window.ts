export interface TimeWindow {
	begin: string;
	end: string;
}
  