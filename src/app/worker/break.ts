import { TimeWindow } from './time-window';

export interface Break extends TimeWindow{
    duration: number;
}
