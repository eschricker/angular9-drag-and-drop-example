import { Component } from '@angular/core';
import { DummyDataService } from './worker/dummy-data.service';
import { Shift, DATE_FORMAT } from './worker/shift';
import { Worker } from './worker/worker';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Moment } from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'drag-and-drop-example';

  workers: Array<Worker>;
  shifts: Array<Shift>;
  currentDate: Moment;

  constructor( public workerDummyData: DummyDataService) {
    console.log('workers:', workerDummyData.workers());
    this.workers = workerDummyData.workers();
    console.log('shifts:', workerDummyData.shifts());
    this.shifts = workerDummyData.shifts();
  }
  dateChanged(event: MatDatepickerInputEvent<Moment>) {
    console.log('event is', event);
    this.currentDate = event.value.clone();
  }
}
